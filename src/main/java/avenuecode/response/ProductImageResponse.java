package avenuecode.response;

import java.util.List;

import avenuecode.entity.Image;
import avenuecode.entity.Product;

/**
 * Objeto criado para agregar duas entitys
 * 
 * Este somente possui construtor e metodos getters
 * Utilizado como resposta (*response)
 * 
 */
public class ProductImageResponse {

	private Product product;
	
	private List<Image> imageList;

	public ProductImageResponse(Product productParameter, List<Image> imageListParameter) {
		
		this.product = productParameter;
		this.imageList = imageListParameter;
	}

	public Product getProduct() {
		return product;
	}

	public List<Image> getImageList() {
		return imageList;
	}
}