package avenuecode.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import avenuecode.entity.Image;
import avenuecode.util.HibernateUtil;

public class ImageService {

	public List<Image> findAll() {

		List<Image> listReturn = new ArrayList<Image>();
		SessionFactory sessionFactory = null;
		Session session = null;
		try {

			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			
			listReturn = (List<Image>) session.createQuery(
					"from " + Image.class.getName()).list();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}

		return listReturn;
	}

	public Image findById(int id) {

		Image image = new Image();
		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();

			image = (Image) session.createQuery(
					"from " + Image.class.getName() + " WHERE id = " + id)
					.uniqueResult();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return image;
	}
	
	public List<Image> findByProductId(int productId) {

		List<Image> imageList = new ArrayList<Image>();
		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();

			imageList = (List<Image>) session.createQuery(
					"from " + Image.class.getName() + " WHERE product_id = " + productId).list();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return imageList;
	}

	public Image save(Image image) {

		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(image);
			session.getTransaction().commit();
			

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return image;
	}

	public boolean delete(int id) {
		
		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			Image image = findById(id);
			session.delete(image);
			session.getTransaction().commit();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.close();
			sessionFactory.close();
		}
		
		return true;
	}

	public Image update(Image Image) {

		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(Image);
			session.getTransaction().commit();


		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return Image;

	}
}
