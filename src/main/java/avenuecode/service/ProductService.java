package avenuecode.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import avenuecode.entity.Product;
import avenuecode.util.HibernateUtil;

public class ProductService {

	public List<Product> findAll() {

		SessionFactory sessionFactory = null;
		Session session = null;
		List<Product> listReturn = new ArrayList<Product>();
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			
			listReturn = (List<Product>) session.createQuery("from " + Product.class.getName()).list();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		
		return listReturn;
	}

	public Product findById(int id) {
		
		SessionFactory sessionFactory = null;
		Session session = null;
		Product product = new Product();
		try {
			
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			product = (Product) session.createQuery("from " + Product.class.getName() 
					+ " WHERE id = " + id).uniqueResult();
			
		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return product;
	}
	
	public Product save(Product product) {

		SessionFactory sessionFactory = null;
		Session session = null;

		try {

			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(product);
			session.getTransaction().commit();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return product;
	}

	public boolean delete(int id) {

		SessionFactory sessionFactory = null;
		Session session = null;
		
		try {

			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			Product product = findById(id);
			session.delete(product);
			session.getTransaction().commit();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.close();
			sessionFactory.close();
		}
		
		return true;
	}

	public Product update(Product product) {

		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(product);
			session.getTransaction().commit();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return product;
		
	}

	@SuppressWarnings("unchecked")
	public List<Product> findChildByParent(int productId) {
		
		List<Product> productList = new ArrayList<Product>();
		SessionFactory sessionFactory = null;
		Session session = null;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			
			productList = (List<Product>) session.createQuery(
					"from " + Product.class.getName() + " WHERE parent = " + productId).list();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			session.close();
			sessionFactory.close();
		}
		return productList;
	}

}
