package avenuecode.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import avenuecode.entity.Image;
import avenuecode.service.ImageService;

@Path("/image")
public class ImageController {

	ImageService imageService = new ImageService();
	
	@GET
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Image> findAll() {

		return imageService.findAll();
	}

	@GET
	@Path("/findById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Image findById(@PathParam("id") int id) {

		return imageService.findById(id);
	}
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Image create(Image image) {

		image = imageService.save(image);
		return image;
	}

	 @GET
	 @Path("/delete/{id}")
	 @Produces(MediaType.TEXT_PLAIN)
	 public String delete(@PathParam("id") int id) {
		 
		 boolean idDeleteSuccess = imageService.delete(id);
		 if(idDeleteSuccess) {
			 return "Deleted successfully";  
		 } else {
			 return "Not Deleted, maybe that will generate orphans";
		 }
	 }
	
	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Image update(@PathParam("id") int id, Image image) {

		Image imageUpdate = imageService.findById(id);
		imageUpdate.setType(image.getType());
		
		image = imageService.update(imageUpdate);

		return image;
	}
	 
//	8) Get set of images for specific product
	@GET
	@Path("/getImagesByProduct/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Image> getImagesByProduct(@PathParam("id") int id) {

		List<Image> imageList = imageService.findByProductId(id);
		return imageList;
	}
}