package avenuecode.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import avenuecode.entity.Image;
import avenuecode.entity.Product;
import avenuecode.response.ProductImageResponse;
import avenuecode.service.ImageService;
import avenuecode.service.ProductService;

@Path("/product")
public class ProductController {

	ProductService productService = new ProductService();
	ImageService imageService = new ImageService();
	
	@GET
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> findAll() {

		return productService.findAll();
	}
	
	@GET
	@Path("/findById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product findById(@PathParam("id") int id) {

		return productService.findById(id);
	}
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Product create(Product product) {

		product = productService.save(product);

		//Load the Parent passed by the user to return the full object  
		if(product.getParent() != null && product.getParent().getId() != null) {
			Product parent = productService.findById(product.getParent().getId());
			product.setParent(parent);
		}

		return product;
	}

	 @GET
	 @Path("/delete/{id}")
	 @Produces(MediaType.TEXT_PLAIN)
	 public String delete(@PathParam("id") int id) {
		 
		 boolean idDeleteSuccess = productService.delete(id);
		 if(idDeleteSuccess) {
			 return "Deleted successfully";  
		 } else {
			 return "Not Deleted, maybe that will generate orphans";
		 }
	 }
	
	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product update(@PathParam("id") int id, Product product) {
		
		Product productUpdate = productService.findById(id);
		productUpdate.setDescription(product.getDescription());
		productUpdate.setName(product.getName());
		productUpdate.setParent(product.getParent());
		
		product = productService.update(productUpdate);

		return product;
	}
	
//	3) Get all products excluding relationships (child products, images) 
	@GET
	@Path("/getAllExcluding")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllExcluding() {

		List<Product> listReturn = productService.findAll();
		for(Product p : listReturn) {
			p.setParent(null);
		}
		
		return listReturn;
	}
	 
//	4) Get all products including specified relationships (child product and/or images)  
	@GET
	@Path("/getAllIncluding/{isParentIncluded}/{isImageIncluded}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductImageResponse> getAllIncluding(@PathParam("isParentIncluded") boolean isParentIncluded,
			@PathParam("isImageIncluded") boolean isImageIncluded) {

		List<ProductImageResponse> responseList = new ArrayList<ProductImageResponse>();
		List<Product> listReturn = productService.findAll();
		
		for(Product p : listReturn) {
			
			List<Image> imagesFromProduct = new ArrayList<Image>();
			if(!isParentIncluded) {
				p.setParent(null);
			}
			
			if(isImageIncluded) {
				imagesFromProduct = imageService.findByProductId(p.getId());
			}
			
			ProductImageResponse response = new ProductImageResponse(p, 
					imagesFromProduct);
			responseList.add(response);
		}

		return responseList;
	}
	
//	5) Same as 3 using specific product identity 
	@GET
	@Path("/getAllExcluding/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getAllExcluding(@PathParam("id") int id) {

		Product product = productService.findById(id);
		product.setParent(null);

		return product;
	}
	
//	6) Same as 4 using specific product identity 
	@GET
	@Path("/getAllIncluding/{id}/{isParentIncluded}/{isImageIncluded}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductImageResponse getAllIncluding(@PathParam("id") int id,
			@PathParam("isParentIncluded") boolean isParentIncluded,
			@PathParam("isImageIncluded") boolean isImageIncluded) {

		List<Image> imagesFromProduct = new ArrayList<Image>();
		Product product = productService.findById(id);
		if(product != null) {
			
			if(!isParentIncluded) {
				product.setParent(null);
			}
			
			if(isImageIncluded) {
				imagesFromProduct = imageService.findByProductId(product.getId());
			}
		}
		
		ProductImageResponse response = new ProductImageResponse(product, imagesFromProduct);
		return response;
	}
	
//	7) Get set of child products for specific product  
	@GET
	@Path("/getChild/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getChild(@PathParam("id") int id) {

		List<Product> productChild = productService.findChildByParent(id);
		return productChild;
	}
}