package avenuecode.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="product")
public class Product implements Serializable {

	private static final long serialVersionUID = -5568634281947186176L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="name", length=100)
	private String name;
	
	@Column(name="description", length=200)
	private String description;

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="parent_product_id", referencedColumnName="id")
	private Product parent;
	
//	@OneToMany(mappedBy="product", fetch=FetchType.LAZY)//, cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
//	private List<Image> images;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}
}