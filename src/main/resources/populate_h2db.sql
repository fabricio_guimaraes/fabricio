--This file is used to create new data
--The schema (database structure) is created by the application at hibernate.cfg.xml
--<property name="hbm2ddl.auto">create</property>
--this file is called here <property name="hbm2ddl.import_files">populate_h2db.sql</property>

INSERT INTO product VALUES (10, 'Lazaro', 'pai', null);
INSERT INTO product VALUES (11, 'Karina', 'irma', 10);
INSERT INTO product VALUES (12, 'Fabricio', 'eu', 10);
INSERT INTO product VALUES (13, 'Gustavo', 'irmao', 10);
INSERT INTO product VALUES (14, 'Camila', 'sobrinha filha karina', 11);
INSERT INTO product VALUES (15, 'Alice', 'neta filha karina neta lazaro', 11);

INSERT INTO image VALUES (10, 'pdf', 10);
INSERT INTO image VALUES (11, 'txt', 10);
INSERT INTO image VALUES (12, 'pdf', 14);
INSERT INTO image VALUES (12, 'txt', 15);