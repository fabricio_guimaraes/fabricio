

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;


import avenuecode.entity.Image;
import avenuecode.entity.Product;
import avenuecode.service.ImageService;
import avenuecode.service.ProductService;

public class ImageTest {

	ImageService imageService = null;
	ProductService productService = null;
	
    @Before
    public void before() {
    	
    	imageService = new ImageService();
    	productService = new ProductService();
    }

    @Test
    public void createImage() {
    	
    	List<Image> imagesBefore = imageService.findByProductId(10);
    	Product product = productService.findById(10);
    	
    	Image image1 = new Image();
    	image1.setType("tipo 1");
    	image1.setProduct(product);
    	image1 = imageService.save(image1);
    	
    	Image image2 = new Image();
    	image2.setType("tipo 22");
    	image2.setProduct(product);
    	image2 = imageService.save(image2);
    	
    	List<Image> imagesAfter = imageService.findByProductId(10);
    	
    	Assert.assertEquals(imagesAfter.size(), imagesBefore.size() + 2);
    }

    @Test
    public void deleteAllImageFromProduct() {
		
		List<Image> imagesFromLazaro = imageService.findByProductId(10);
		Assert.assertNotNull(imagesFromLazaro);

		for(Image i : imagesFromLazaro) {
			imageService.delete(i.getId());
		}
		imagesFromLazaro = imageService.findByProductId(10);
		Assert.assertNotNull(imagesFromLazaro);
		Assert.assertEquals(0, imagesFromLazaro.size());
	}
	
    
}