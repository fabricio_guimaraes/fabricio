
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;


import avenuecode.entity.Product;
import avenuecode.service.ProductService;

public class ProductTest {

	ProductService productService = null;
	
	@Before
	public void before() {
		
		productService = new ProductService();
	}
	
	@Test
	public void productQuantity() {
		
		List<Product> listReturn = new ArrayList<Product>();
		listReturn = productService.findAll();

		Assert.assertNotNull(listReturn);
		Assert.assertEquals(6, listReturn.size());
	}

	@Test
	public void createProduct() {
		
		 Product parent = new Product();
		 parent.setName("Lazaro");
		 parent.setDescription("Parent with 2 child");
		 parent.setParent(null);
		 parent = productService.save(parent);
		
		 Product primogenita = new Product();
		 primogenita.setName("Karina");
		 primogenita.setDescription("Primogenita");
		 primogenita.setParent(parent);
		 primogenita = productService.save(primogenita);
		
		 Product cacula = new Product();
		 cacula.setName("Fabricio");
		 cacula.setDescription("Cacula");
		 cacula.setParent(parent);
		 cacula = productService.save(cacula);
		
		 // Todos filhos do mesmo pai
		 Assert.assertEquals(primogenita.getParent().getId(), parent.getId());
		 Assert.assertEquals(cacula.getParent().getId(), parent.getId());
		
		 // Pai possui 2 filhos
		 List<Product> childrenFromParent = productService.findChildByParent(parent.getId());
		 Assert.assertNotNull(childrenFromParent);
		 Assert.assertEquals(2, childrenFromParent.size());
	}
	
	@Test
	public void deleteProduct() {
		
		List<Product> listOld = productService.findAll();
		int productQuantityOld = listOld.size();
		
		Product parent = new Product();
		parent.setName("Product to be deleted");
		parent.setDescription("This is just a temporary product");
		parent.setParent(null);
		parent = productService.save(parent);
		int actualQuantity = productQuantityOld + 1;
		
		List<Product> listNew = productService.findAll();
		int productQuantityNew = listNew.size();
		
		//see if it was inserted
		Assert.assertEquals(actualQuantity, productQuantityNew);
		
		productService.delete(parent.getId());
		
		listNew = productService.findAll();
		productQuantityNew = listNew.size();
		
		//see if it was deleted
		Assert.assertEquals(productQuantityOld, productQuantityNew);
	}
	
	@Test
	public void getAllChildrenFromProduct() {
		
//		she has 2 children
		List<Product> childrenFromKarina = productService.findChildByParent(11);
		Assert.assertNotNull(childrenFromKarina);
		Assert.assertEquals(2, childrenFromKarina.size());

//		now they will be child of the Product Fabricio, id 12
		for(Product p : childrenFromKarina) {
			p.setDescription("filha do id 12");
			Product product10 = productService.findById(12);
			p.setParent(product10);
			productService.update(p);
		}

		List<Product> childrenFromFabricio = productService.findChildByParent(12);
		Assert.assertNotNull(childrenFromFabricio);
		Assert.assertEquals(2, childrenFromFabricio.size());
	}
	
	 @After
	 public void after() {
	 
	 }

}