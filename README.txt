Foi utilizado para esta API:
Java 1.8;
Apache Maven;
h2db in memory;
JPA/Hibernate
Ubuntu 14.04 Linux;
Eclipse;
bitbucket;

Inicialmente é necessario rodar a API para a criação da estrutura do banco de dados e a inserção de dados.
	hibernate.cfg.xml
		<property name="hbm2ddl.auto">create</property>
		<property name="hbm2ddl.import_files">populate_h2db.sql</property>
Para executar os métodos da API, pode-se utilizar o POSTMAN (add-on do Google Chrome) ou o HTTP Requester (add on do Mozilla Firefox).

***TESTS***
Para executar o suite de testes utilize.
	$ mvn test

---classes---
	SuiteTest.java
	ProductTest.java
	ImageTest.java


***API***
Para inicializar a aplicação API, basta entrar na pasta e executar: 

	$ mvn jetty:run


1) Create, update and delete products
----------------------------------------------------------------------------
METODO: CREATE TIPO: POST 
URL: http://localhost:8080/product/create
PARAMETRO: 
	Adicionar no BODY da requisicao um objeto Product
EXEMPLO: 
	--BODY
	{
		"name": "Lazaro",
		"description": "pai"
	}
	{
		"name": "Karina",
		"description": "irma",
		"parent": {"id":1}
	}
	{
		"name": "Fabricio",
		"description": "eu",
		"parent": {"id":1}
	}

	--BODY
RETORNO: APPLICATION_JSON - Objeto salvo. A fim de retornar o ID gerado (por exemplo, colocar em um objeto Image)
----------------------------------------------------------------------------

----------------------------------------------------------------------------
METODO: UPDATE  TIPO: POST  
URL: http://localhost:8080/product/update/{id}
PARAMETRO:
	Adicionar no BODY da requisicao um objeto Product. O {id} do objeto a sofrer update esta sendo passado pela URL, a fim de mostrar um POST com um parametro na URL.
EXEMPLO: 
	--BODY
	http://localhost:8080/product/update/3
	{
		"name": "Alice",
		"description": "Atualizando para ser neto do Lazaro",
		"parent": {"id":2}
	}
	--BODY
RETORNO: APPLICATION_JSON - Objeto alterado.
----------------------------------------------------------------------------

----------------------------------------------------------------------------
METODO: DELETE  TIPO: GET  
URL: http://localhost:8080/product/delete/{id}
PARAMETRO: {id} na requisicao do GET a sofrer o delete
EXEMPLO: http://localhost:8080/product/delete/3
RETORNO: TEXT_PLAIN - String com mensagem de sucesso/erro
** 
ao tentar deletar um Product que contenha filhos, pela restrição do BD, retornará erro como exemplo:
1)se Houver Product parent
"Caused by: org.h2.jdbc.JdbcBatchUpdateException: Referential integrity constraint violation: "FKED8DCCEFF1CF1AAE: PUBLIC.PRODUCT FOREIGN KEY(PARENT) REFERENCES PUBLIC.PRODUCT(ID) (1)"; SQL statement:"

2) se houver Image child
Caused by: org.h2.jdbc.JdbcSQLException: Referential integrity constraint violation: "FK5FAA95BC41C47CF: PUBLIC.IMAGE FOREIGN KEY(ID_PRODUCT) REFERENCES PUBLIC.PRODUCT(ID) (3)"; SQL statement:
**
----------------------------------------------------------------------------

2) Create, update and delete images
----------------------------------------------------------------------------
METODO: CREATE TIPO: POST 
URL: http://localhost:8080/image/create
PARAMETRO: 
	Adicionar no BODY da requisicao um objeto Image
	--BODY
EXEMPLO: POST http://localhost:8080/image/create
	{
		"type": "pdf",
		"product": {"id":1}
	}

	{
		"type": "txt",
		"product": {"id":2}
	}
	--BODY
RETORNO: APPLICATION_JSON - Objeto salvo. A fim de retornar o ID gerado.
----------------------------------------------------------------------------

----------------------------------------------------------------------------
METODO: UPDATE  TIPO: POST  
URL: http://localhost:8080/image/update/{id}
PARAMETRO:
	Adicionar no BODY da requisicao um objeto Product. O {id} do objeto a sofrer update esta sendo passado pela URL, a fim de mostrar um POST com um parametro na URL.

EXEMPLO: POST http://localhost:8080/image/update/2
	--BODY
	{
		"type": "text para 1",
		"product": {"id":1}
	}
	--BODY
RETORNO: APPLICATION_JSON - Objeto alterado
----------------------------------------------------------------------------

----------------------------------------------------------------------------
METODO: DELETE  TIPO: GET  
URL: http://localhost:8080/image/delete/{id}
PARAMETRO: {id} na requisicao do GET a sofrer o delete
EXEMPLO: http://localhost:8080/image/delete/2
RETORNO: TEXT_PLAIN - String com mensagem de sucesso/erro
----------------------------------------------------------------------------

3) Get all products excluding relationships (child products, images) 
----------------------------------------------------------------------------
METODO: Get all products excluding relationships TIPO: GET  
URL: http://localhost:8080/product/getAllExcluding
PARAMETRO:
EXEMPLO: http://localhost:8080/product/getAllExcluding
RETORNO: APPLICATION_JSON - List<Product>
----------------------------------------------------------------------------


4) Get all products including specified relationships (child product and/or images) 
----------------------------------------------------------------------------
METODO: Get all products including specified relationships TIPO: GET  
URL: http://localhost:8080/product/getAllIncluding/{isChildIncluded}/{isImageIncluded}
PARAMETRO: 
	{isChildIncluded} - true/false se deve adicionar os child Product. 
	{isImageIncluded} - true/false se deve adicionar as Image.
EXEMPLO: http://localhost:8080/product/getAllIncluding/true/true
RETORNO: APPLICATION_JSON - List<ProductImageResponse>, com ou sem child product / images
OBSERVACAO: Objeto ProductImageResponse, "response", criado para agrupar os objetos. 
*Criado assim, pois os mapeamentos nas entidades dos objetos, como abaixo:
@ManyToOne(fetch=FetchType.EAGER)
@JoinColumn(name="parent_product_id", referencedColumnName="id")
private Product parent;
@OneToMany(mappedBy="product", fetch=FetchType.LAZY)//, cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
private List<Image> images;
*estavam com LazyLoadException. (Provavelmente devido as transações de sessão nas classes ProductService e ImageService)
----------------------------------------------------------------------------


5) Same as 3 using specific product identity 
----------------------------------------------------------------------------
METODO: Get all products excluding relationships, with an id from Product as parameter TIPO: GET  
URL: http://localhost:8080/product/getAllExcluding/{id}
PARAMETRO: {id} do Produto
EXEMPLO: http://localhost:8080/product/getAllExcluding/1
RETORNO: APPLICATION_JSON - List<Product>
----------------------------------------------------------------------------


6) Same as 4 using specific product identity 
----------------------------------------------------------------------------
METODO: Get all products including specified relationships TIPO: GET  
URL: http://localhost:8080/product/getAllIncluding/{id}/{isChildIncluded}/{isImageIncluded}
PARAMETRO: 
	{id} do Produto, product identity 
	{isChildIncluded} - true/false se deve adicionar os child product. 
	{isImageIncluded} - true/false se deve adicionar as image
EXEMPLO: http://localhost:8080/product/getAllIncluding/1/true/true
RETORNO: APPLICATION_JSON - ProductImageResponse, com ou sem child product / images (veja item 4)
----------------------------------------------------------------------------


7) Get set of child products for specific product 
----------------------------------------------------------------------------
METODO: Get set of child products for specific product  TIPO: GET  
URL: http://localhost:8080/product/getChild/{id}
PARAMETRO: 
	{id} do Produto, product identity 
EXEMPLO: http://localhost:8080/product/getChild/2
RETORNO: APPLICATION_JSON - List<Product>
OBSERVACAO: por conta do mapeamento, um ciclo ocorreu. Buscamos pelo parent 2, e assim veio seu filho e ele mesmo.
  {
        "id": 3,
        "name": "Alice",
        "description": "Atualizando para ser neto do Lazaro",
        "parent": {
            "id": 2,
            "name": "Karina",
            "description": "irma",
            "parent": {
                "id": 1,
                "name": "Lazaro",
                "description": "pai",
                "parent": null
            }
        }
    }
]
----------------------------------------------------------------------------

8) Get set of images for specific product
----------------------------------------------------------------------------
METODO: Get set of images for specific product TIPO: GET  
URL: http://localhost:8080/image/getImagesByProduct/{id}
PARAMETRO: 
	{id} do Produto, product identity 
EXEMPLO: http://localhost:8080/image/getImagesByProduct/1
RETORNO: APPLICATION_JSON - List<Image>
----------------------------------------------------------------------------


Melhorias: As classes ProductService e ImageService estão fechando a conexão com o BD, assim não é possível usar o FetchType.LAZY, somente o fetch=FetchType.EAGER, fazendo que sempre tragam os filhos.
